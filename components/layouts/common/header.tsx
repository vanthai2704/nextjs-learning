import { useState } from 'react'
import { Button, Divider, Dropdown, Image, MenuProps, Space } from 'antd'
import { Menu } from 'antd'
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
  DownOutlined,
  ProfileOutlined,
  SearchOutlined,
} from '@ant-design/icons'
type MenuItem = Required<MenuProps>['items'][number]

export interface IHeaderProps {}

export function Header(props: IHeaderProps) {
  const [openKeys, setOpenKeys] = useState([''])
  const rootSubmenuKeys = ['sub1', 'sub2', 'sub4']

  const onClick: MenuProps['onClick'] = (e) => {
    console.log('click', e)
  }

  function getItem(
    label: React.ReactNode,
    key?: React.Key | null,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group'
  ): MenuItem {
    return {
      key,
      icon,
      children,
      label,
      type,
    } as MenuItem
  }
  const items: MenuItem[] = [
    getItem('Navigation One', 'sub1', <MailOutlined />, [
      getItem('Item 1', null, null, [getItem('Option 1', '1'), getItem('Option 2', '2')], 'group'),
      getItem('Item 2', null, null, [getItem('Option 3', '3'), getItem('Option 4', '4')], 'group'),
    ]),

    getItem('Navigation Two', 'sub2', <AppstoreOutlined />, [
      getItem('Option 5', '5'),
      getItem('Option 6', '6'),
      getItem('Submenu', 'sub3', null, [getItem('Option 7', '7'), getItem('Option 8', '8')]),
    ]),

    getItem('Navigation Three', 'sub4', <SettingOutlined />, [
      getItem('Option 9', '9'),
      getItem('Option 10', '10'),
      getItem('Option 11', '11'),
      getItem('Option 12', '12'),
    ]),
  ]

  const itemsStickyMenu: MenuProps['items'] = [
    {
      label: 'Navigation One',
      key: 'mail',
      icon: <MailOutlined />,
    },
    {
      label: 'Navigation Two',
      key: 'app',
      icon: <AppstoreOutlined />,
      disabled: true,
    },
    {
      label: 'Navigation Three - Submenu',
      key: 'SubMenu',
      icon: <SettingOutlined />,
      children: [
        {
          type: 'group',
          label: 'Item 1',
          children: [
            {
              label: 'Option 1',
              key: 'setting:1',
            },
            {
              label: 'Option 2',
              key: 'setting:2',
            },
          ],
        },
        {
          type: 'group',
          label: 'Item 2',
          children: [
            {
              label: 'Option 3',
              key: 'setting:3',
            },
            {
              label: 'Option 4',
              key: 'setting:4',
            },
          ],
        },
      ],
    },
    {
      label: (
        <a href="https://ant.design" target="_blank" rel="noopener noreferrer">
          Navigation Four - Link
        </a>
      ),
      key: 'alipay',
    },
  ]

  const onOpenChange: MenuProps['onOpenChange'] = (keys) => {
    console.log(keys)

    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1)
    if (rootSubmenuKeys.indexOf(latestOpenKey!) === -1) {
      setOpenKeys(keys)
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : [])
    }
  }

  const [current, setCurrent] = useState('mail')

  const onClick2: MenuProps['onClick'] = (e) => {
    console.log('click ', e)
    setCurrent(e.key)
  }

  return (
    <div>
      <div className="grid grid-cols-12 h-20 content-center">
        <div className="col-span-3">
          <Image src="imgs/theme/logo.png" alt="" />
        </div>
        <div className="col-span-9 flex justify-end items-baseline">
          <Dropdown
            overlay={
              <Menu
                onClick={onClick}
                openKeys={openKeys}
                onOpenChange={onOpenChange}
                mode="vertical"
                items={items}
              />
            }
            placement="bottom"
            arrow={{ pointAtCenter: true }}
          >
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                Layout
                <DownOutlined />
              </Space>
            </a>
          </Dropdown>
          <Button icon={<ProfileOutlined />}>Document</Button>
          <Button icon={<SearchOutlined />}>Search</Button>
          <Button type="primary">Buy Now</Button>
        </div>
      </div>
      <Divider plain></Divider>

      <div className="header-sticky">
        <Menu
          onClick={onClick2}
          selectedKeys={[current]}
          mode="horizontal"
          items={itemsStickyMenu}
        />
        ;
      </div>
    </div>
  )
}
