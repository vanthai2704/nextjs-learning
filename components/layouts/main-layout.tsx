import { LayoutProps } from '@/models/common'
// import { Layout, Menu, Breadcrumb } from 'antd'
// const { Header, Footer, Sider, Content } = Layout
import Link from 'next/link'
import * as React from 'react'
import { Button } from 'antd'
import { Header } from './common'

// import { Footer, Header } from './common'
const style = {
  width: '1110px',
}

export function MainLayout({ children }: LayoutProps) {
  return (
    <div className="mx-auto container">
      <Header></Header>
      {children}
    </div>
  )
}
