import { useRouter } from 'next/router'
import * as React from 'react'

export interface IAppProps {}

export default function App(props: IAppProps) {
  const router = useRouter()
  const { postId, testParam } = router.query
  function goBack() {
    router.back()
  }
  return (
    <div>
      {postId}, {testParam}
      <button onClick={goBack}>Back</button>
    </div>
  )
}
