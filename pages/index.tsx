import { MainLayout } from '@/components/layouts'
import { NextPageWithLayout } from '@/models'
import type { NextPage } from 'next'

import { useRouter } from 'next/router'
import styles from '../styles/Home.module.css'

const Home: NextPageWithLayout = () => {
  const router = useRouter()
  function goToPosts() {
    router.push({
      pathname: '/posts/[postId]',
      query: {
        postId: 123,
        testParam: 'ok',
      },
    })
  }
  return (
    <div className={styles.container}>
      <div>
        <h1 className="text-3xl font-bold underline">Hello world!</h1>
        <button onClick={goToPosts}>Go to About</button>
      </div>
      <h1>this is index</h1>
    </div>
  )
}

Home.Layout = MainLayout

export default Home
