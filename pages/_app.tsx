import '../styles/globals.css'
import 'antd/dist/antd.css'
import type { AppProps } from 'next/app'

// import { MainLayout } from '../components/layouts'
import { AppPropsWithLayout } from '@/models/common'
import { EmptyLayout } from '@/components/layouts'

// import MainLayout from '../components/layouts/main'

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const Layout = Component.Layout ?? EmptyLayout
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}

export default MyApp
