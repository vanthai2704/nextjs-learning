import { Product } from '@/models'
import { GetStaticProps } from 'next'
import * as React from 'react'

export interface IProductPageProps {
  data: Product[]
}

export default function ProductPage(props: IProductPageProps) {
  const productList = props.data
  return (
    <div>
      <h3>this is about page</h3>
      <ol>
        {productList.map((l) => (
          <li key={l.id}>{l.name}</li>
        ))}
      </ol>
    </div>
  )
}

export const getStaticProps: GetStaticProps = async (ctx) => {
  const response = await fetch('https://62d3cabb81cb1ecafa7139dc.mockapi.io/api/hello/products') // your fetch function here

  return {
    props: {
      data: await response.json(),
    },
  }
}
